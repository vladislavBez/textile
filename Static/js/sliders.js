$(document).ready(function () {

    $("#main-slider").slick({
        prevArrow: "<div class='slick-arrow slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-arrow slick-next-arrow'></div>",
    });

    $("#small-slider").slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow: "<div class='slick-arrow slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-arrow slick-next-arrow'></div>",
    });

});